import org.junit.Assert;
import org.junit.Test;

import java.util.function.Function;

public class CrossesAndZeroes {

    static class Turn {
        Coordinate coordinate;

        Party party;

        Turn(Coordinate coordinate, Party party) {
            this.coordinate = coordinate;
            this.party = party;
        }
    }

    static class Coordinate {
        int x;
        int y;

        public Coordinate(int x, int y) {
            this.x = x;
            this.y = y;
        }

        // x . . x
        // . x x .
        // . x x .
        // x . . x
        public boolean isDiagonal(int boardSize) {
            return x == y || x == boardSize - y - 1;
        }
    }

    enum Party {
        ZERO,
        CROSS
    }

    boolean solution(Party[][] board, Turn turn) {

        makeTurn(board, turn);

        final Party party = turn.party;

        // check horizontal line
        if (check(board, party, i -> turn.coordinate.y, Function.identity()))
            return true;

        // check vertical line
        if (check(board, party, Function.identity(), i -> turn.coordinate.x))
            return true;

        // if turn lays out of a diagonal, return false immediately
        final int N = board.length;
        if (!turn.coordinate.isDiagonal(N)) {
            return false;
        }

        // check first diagonal
        if (check(board, party, Function.identity(), Function.identity())) {
            return true;
        }

        // lastly, check second diagonal
        return check(board, party, Function.identity(), i -> N - i - 1);
    }

    private boolean check(Party[][] board, Party party, Function<Integer, Integer> yCoord, Function<Integer, Integer> xCoord) {
        final int N = board.length;
        int count = 0;

        for (int i = 0; i < N; i++) {
            final int y = yCoord.apply(i);
            final int x = xCoord.apply(i);
            if (board[y][x] == party) {
                count++;
            }
        }

        return count == N;
    }

    private void makeTurn(Party[][] board, Turn turn) {
        board[turn.coordinate.y][turn.coordinate.x] = turn.party;
    }

    @Test
    public void test1() {
        CrossesAndZeroes task = new CrossesAndZeroes();

        Party[][] board = {
                {null, null, null},
                {null, null, null},
                {null, null, null},
        };

        Assert.assertEquals(false, task.solution(board, new Turn(new Coordinate(0, 0), Party.ZERO)));
    }

    @Test
    public void test_horizontal() {
        CrossesAndZeroes task = new CrossesAndZeroes();

        Party[][] board = {
                {null, null, null},
                {Party.ZERO, Party.ZERO, null},
                {null, null, null},
        };

        Assert.assertEquals(true, task.solution(board, new Turn(new Coordinate(2, 1), Party.ZERO)));
    }

    @Test
    public void test_vertical() {
        CrossesAndZeroes task = new CrossesAndZeroes();

        Party[][] board = {
                {null, null, null},
                {Party.ZERO, null, null},
                {Party.ZERO, null, null},
        };

        Assert.assertEquals(true, task.solution(board, new Turn(new Coordinate(0, 0), Party.ZERO)));
    }

    @Test
    public void test_diagonal1() {
        CrossesAndZeroes task = new CrossesAndZeroes();

        Party[][] board = {
                {null, null, null},
                {null, Party.ZERO, null},
                {Party.ZERO, null, null},
        };

        Assert.assertEquals(true, task.solution(board, new Turn(new Coordinate(2, 0), Party.ZERO)));
    }

    @Test
    public void test_diagonal2() {
        CrossesAndZeroes task = new CrossesAndZeroes();

        Party[][] board = {
                {null, null, null},
                {null, Party.ZERO, null},
                {null, null, Party.ZERO,},
        };
        Assert.assertEquals(true, task.solution(board, new Turn(new Coordinate(0, 0), Party.ZERO)));
    }
}
