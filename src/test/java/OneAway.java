import org.junit.Assert;
import org.junit.Test;

/**
 * There are three types of edits that can be performed on strings: insert a character, remove a character,
 * or replace a character. Given two strings, write a function to check if they are one edit (or zero edits) away.
 * <p>
 * EXAMPLE
 * <p>
 * nice, niece -> true
 * crow, cow -> true
 * bean, been -> true
 * o, odd -> false
 */
public class OneAway {

    private boolean solution(String s1, String s2) {
        final int len1 = s1.length();
        final int len2 = s2.length();

        if (len1 > len2) {
            return solution(s2, s1);
        }

        if (len2 - len1 > 1) {
            return false;
        }

        int s1Idx = 0, s2Idx = 0;
        int diffCount = 0;
        while (s1Idx < len1 && s2Idx < len2 && diffCount < 2) {
            char c1 = s1.charAt(s1Idx);
            char c2 = s2.charAt(s2Idx);

            if (c1 != c2) {
                diffCount++;
                if (len1 < len2) {
                    s2Idx++;
                    continue;
                }
            }

            s1Idx++;
            s2Idx++;
        }

        return diffCount == 1 || diffCount == 0;
    }


    @Test
    public void test1() {
        OneAway task = new OneAway();
        Assert.assertTrue(task.solution("nice", "niece"));
        Assert.assertTrue(task.solution("niece", "nice"));
    }

    @Test
    public void test2() {
        OneAway task = new OneAway();
        Assert.assertTrue(task.solution("crow", "cow"));
        Assert.assertTrue(task.solution("cow", "crow"));
    }

    @Test
    public void test3() {
        OneAway task = new OneAway();
        Assert.assertTrue(task.solution("bean", "been"));
        Assert.assertTrue(task.solution("been", "bean"));
    }

    @Test
    public void test4() {
        OneAway task = new OneAway();
        Assert.assertFalse(task.solution("odd", "o"));
        Assert.assertFalse(task.solution("o", "odd"));
    }

    @Test
    public void test4_2() {
        OneAway task = new OneAway();
        Assert.assertFalse(task.solution("ave", "even"));
        Assert.assertFalse(task.solution("even", "ave"));
    }

    @Test
    public void test4_3() {
        OneAway task = new OneAway();
        Assert.assertTrue(task.solution("aven", "even"));
        Assert.assertTrue(task.solution("even", "aven"));
    }
}
