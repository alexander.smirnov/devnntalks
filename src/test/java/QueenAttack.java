import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class QueenAttack {

    // Complete the queensAttack function below.
    static int queensAttack(int n, int k, int r_q, int c_q, int[][] obstacles) {
        BoardState boardState = new BoardState(n, r_q, c_q, obstacles);
        return boardState.queensAttack();
    }

    static class BoardState {

        final int n;
        final int queenRow;
        final int queenColumn;
        Set<Coordinate> obstaclesSet;

        BoardState(int n, int queenRow, int queenColumn, int[][] obstacles) {
            this.n = n;
            this.queenRow = queenRow;
            this.queenColumn = queenColumn;
            this.obstaclesSet = getCoordinates(obstacles);
        }

        private Set<Coordinate> getCoordinates(int[][] obstacles) {
            return Arrays.stream(obstacles)
                    .filter(entry -> entry.length == 2)
                    .map(entry -> new Coordinate(entry[0], entry[1]))
                    .collect(Collectors.toSet());
        }

        public int queensAttack() {

            int result = 0;

            for (Direction direction : Direction.QUEENS) {
                result += countForDirection(direction);
            }

            return result;
//            return Direction.QUEENS.stream().mapToInt(this::countForDirection).sum();
        }

        private int countForDirection(Direction direction) {
            int count = 0;

            int row = direction.nextRow(queenRow);
            int col = direction.nextColumn(queenColumn);
            while (areCoordinatesValid(row, col) && !isObstacle(row, col)) {
                count++;
                row = direction.nextRow(row);
                col = direction.nextColumn(col);
            }
            return count;
        }

        boolean areCoordinatesValid(int row, int col) {
            return isValidRow(row) && isValidColumn(col);
        }

        boolean isValidRow(int row) {
            return row >= 1 && row <= n;
        }

        boolean isValidColumn(int column) {
            return column >= 1 && column <= n;
        }

        boolean isObstacle(int row, int column) {
            return obstaclesSet.contains(new Coordinate(row, column));
        }
    }

    static class Coordinate {
        int row;
        int column;

        public Coordinate(int row, int column) {
            this.row = row;
            this.column = column;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Coordinate that = (Coordinate) o;
            return row == that.row &&
                    column == that.column;
        }

        @Override
        public int hashCode() {
            return Objects.hash(row, column);
        }
    }

    static class Direction {
        private static final Function<Integer, Integer> PLUS_ONE = arg -> arg + 1;
        private static final Function<Integer, Integer> MINUS_ONE = arg -> arg - 1;

        public static final Direction LEFT = new Direction(Function.identity(), MINUS_ONE);
        public static final Direction RIGHT = new Direction(Function.identity(), PLUS_ONE);
        public static final Direction UP = new Direction(PLUS_ONE, Function.identity());
        public static final Direction DOWN = new Direction(MINUS_ONE, Function.identity());
        public static final Direction LEFT_UP = new Direction(PLUS_ONE, MINUS_ONE);
        public static final Direction LEFT_DOWN = new Direction(MINUS_ONE, MINUS_ONE);
        public static final Direction RIGHT_UP = new Direction(PLUS_ONE, PLUS_ONE);
        public static final Direction RIGHT_DOWN = new Direction(MINUS_ONE, PLUS_ONE);

        public static List<Direction> QUEENS = Arrays.asList(
                LEFT, RIGHT, UP, DOWN, LEFT_UP, LEFT_DOWN, RIGHT_UP, RIGHT_DOWN);

        final Function<Integer, Integer> rowFunction;
        final Function<Integer, Integer> columnFunction;

        private Direction(Function<Integer, Integer> rowFunction, Function<Integer, Integer> columnFunction) {
            this.columnFunction = columnFunction;
            this.rowFunction = rowFunction;
        }

        int nextRow(int row) {
            return rowFunction.apply(row);
        }

        int nextColumn(int column) {
            return columnFunction.apply(column);
        }
    }

    @Test
    public void test_4x4_corner_no_obstacles() {
        int[][] obstacles = {};
        Assert.assertEquals(9, queensAttack(4, 0, 4, 4, obstacles));
    }

    @Test
    public void test_5x5_3_obstacles() {
        int[][] obstacles = {{5, 5}, {4, 2}, {2, 3}};
        Assert.assertEquals(10, queensAttack(5, 3, 4, 3, obstacles));
    }

    @Test
    public void test_1x1_no_obstacles() {
        int[][] obstacles = {{}};
        Assert.assertEquals(0, queensAttack(1, 0, 1, 1, obstacles));
    }
}
